# Shoubu
The fast-paced simple game that's actually really fun!

This repository provides various tools for bots and humans to learn and play
Shoubu. Status: ❌- Not started, 🟡 - WIP, ✅- stable

 - 🟡 `shoubu-lib`: Basic structures to implement the rules
 - ❌ `shoubuctl`: An intermediary agent to run a Shoubu game between two players
 - ❌ `shoubu-display`: An interface for humans to play more easily
 - ❌ `shinobu-xxx`: A flavour of our Shoubu-playing bot

To play between two people, use `shoubu-display` which'll automatically startup
a `shoubuctl` under the hood. Use `shoubuctl` directly to play two bots against
each other, without the display overhead.

All tools, except `shoubu-lib` which can't be used on its own, support the
`--help` flag for more information. Using `help` often works in interactive
instances too

## Layout
Our game supports two players `Black = 1` and `White = -1`. Coordinates are
referenced by 3 axis, with the first indexing like [cartesian
quadrants](https://en.wikipedia.org/wiki/Quadrant_(plane_geometry)). Everything
is 1 indexed. Coordinates may look like:

```bash
# Template: (board, row, column)
(1, 1, 4)  # Top right corner of visual below. Black's right home board
(3, 4, 1)  # Bottom left corner of visual below. White's left home board
```

```
                     Black's Home Boards
           @@@@               |               @
          @    @              |              @@
  left         @              |             @ @    right
  board     @@@               |               @    board
           @                  |               @
          @@@@@@@             |            @@@@@@@
ROPE==========================|==========================ROPE
           @@@@               |            @     @
          @    @              |            @     @
  left         @              |            @     @  right
  board     @@@               |            @@@@@@@  board
               @              |                  @
          @    @              |                  @
           @@@@               |                  @
                     White's Home Boards
```

Boards 1 and 4 are "right boards". Boards 2 and 3 are "left boards", as defined
in `shoubu-lib`. Black's home boards are 1 and 2, while White's are 3 and 4

Each board is then indexed by rows then columns

```
     │ 1 │ 2 │ 3 │ 4 │
    ─┼───┼───┼───┼───┤
    1│   │   │   │   │
    ─┼───┼───┼───┼───┤
    2│   │   │   │   │
    ─┼───┼───┼───┼───┤
    3│   │   │   │   │
    ─┼───┼───┼───┼───┤
    4│   │   │   │   │
     └───┴───┴───┴───┘
```

## Communication Protocol
`shoubu-ctl` defines the communicate protocol. If you're a human reading this
and just want to play, `shoubu-display` already handles the underlying bits, so
there's no need to worry about it. For bots and humans making bots, the protocol
is divided into sections

A move is formatted almost identically to the coordinate definition above,
except all the numbers are together. It matches the regex `[1-4]{12}`

Consider this example, where white makes a passive move on board 3 and an
aggressive move on board 1. The bot will send the following message, terminated
with a single `\n`:

```
move 332314141123
```

Dissecting this for human eyes

```
     PASSIVE MOVE                   AGGRESSIVE MOVE
    FROM        TO                  FROM        TO
   (3,3,2) -> (3,1,4)──────┐  ┌────(1,4,1) -> (1,2,3)
   └──┬──┘                 │  │               └──┬──┘
      └─────────────────┐  │  │  ┌───────────────┘
                       ┌┴┐┌┴┐┌┴┐┌┴┐
                  move 332314141123

    Passive move on board 3: (3,2) -> (1,4)
 Aggressive move on board 1: (4,1) -> (2,3)

Both tiles are moving diagonally 2 squares to the top right. Obviously both FROM
and TO must remain on the same board
```

#### Bot messages
These messages are sent by the bot to the stdout. A response from `shoubuctl`
will come in through the stdin

| Command | Response | Description |
|---------|----------|-------------|
|`help`   | Help message | Prints a help message of all valid commands |
|`move {move}` | `success` or `fail` | Attempts to play the move. `fail` if it's an illegal move |
|`resign` | None | Bot resigns from the game. Should only happen with no possible moves |

#### shoubuctl messages
These messages are sent to bots by `soubuctl` through stdin. Bots should be
actively monitoring their input for these commands as time starts once the
message is done sending

| Message | Description |
|---------|-------------|
|`mkmove` | Requests the bot to make a move. Bots should keep trying until they get a `success` message |
|`settime {seconds}` | Sets the timelimit for moves. Taking longer results in a random move being played |
|`reset` | Tells the bot to reset the game |

#### Request commands
These commands can be requested manually from `shoubuctl`. `shoubu-display`
wraps around these for human players

| Command | Description |
|---------|-------------|
|`mkmove` | Requests the next move. `shoubuctl` will print the resulting move |
|`settime {seconds}` | Sets the timelimit for moves. Taking longer results in a random move being played |
|`reset` | Tells the bot to reset the game |
|`play {n}` | Shoubuctl will `mkmove` automatically until a win for `n` wins, then prints cumulative end-game statistics |

#### Spelling...
I spell it Shoubu with that first `u`. The official spelling is Shōbu, though
that unicode `ō` is too hard to write, so most online spellings use Shobu. Use
that one when searching for information. I retain it since Shoubu would be the
Latin rendition of the Japanese word "battle" or "victory", while shobu means to
dispose of garbage... I feel this game isn't garbage, so the `u` stays
