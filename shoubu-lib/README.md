# Shōbu's Core
This library implements the core rules and structures required to play Shōbu.
Currently it's a rust-only API, intended for bots and human-targeted interfaces

## Building
`shoubu` is well documented. Open the documentation with:

```bash
cargo doc --open
```
