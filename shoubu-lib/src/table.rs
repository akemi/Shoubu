//! Defines the [Table] and [boards](Board), representing a snapshot of the game
//!
//! Some public functions here aren't safe to use directly and may result in a malformed table.
//! Read the comments first, especially if the function doesn't return a Result. The less-safe API
//! may be appealing to bots looking to reduce overhead, though it's likely not much slower to use
//! the safer versions
use super::*;

/// A collection of 4 boards in order, preserving a snapshot of the game
///
/// There is no display API for a table. Interfaces are encouraged to develop their own by
/// inspecting the fields directly
#[derive(Clone)]
pub struct Table {
    pub boards: [Board; 4],
}

pub type TableHash = u128;

/// A single 4x4 Shoubu board
///
/// Boards cannot validate a [Move], since they cannot see the mirrored move, so their API works
/// with [Mv]. Boards also contain most of the unchecked API for speed-seeking bots.
///
/// There is no display API for a board. Interfaces are encouraged to develop their own by
/// inspecting the fields directly
#[derive(Clone)]
pub struct Board {
    pub tiles: [Tile; 16],
    pub index: usize,
}

impl Board {
    /// Creates a new **EMPTY** board. Use `.init()` for a starting board
    pub fn new(index: usize) -> Self {
        Self {
            tiles: [Tile::Empty; 16],
            index,
        }
    }

    /// Initializes a board with both sides in this starting positions
    pub fn init(index: usize) -> Self {
        let mut b = Self::new(index);

        for c in 0..4 {
            b.tiles[c] = Tile::Black;
            b.tiles[12+c] = Tile::White;
        }

        b
    }

    /// Returns the color of the player who won. None if nobody's own yet
    pub fn winner(&self) -> Option<PlayerColor> {
        if self.nb_black() == 0 {
            Some(PlayerColor::White)
        } else if self.nb_white() == 0 {
            Some(PlayerColor::Black)
        } else {
            None
        }
    }

    /// Returns true if the board is on the "left" of the table
    ///
    /// This is not meant to be used for visual displays, it's a way of telling appart the light
    /// and dark boards to check for mirroring moves
    pub fn is_left_board(&self) -> bool {
        self.index == 2 || self.index == 3
    }

    /// Returns true if this is one of the black player's two home boards
    ///
    /// Black's home boards are 1 and 2, while White's are 3 and 4
    pub fn is_black_homeboard(&self) -> bool {
        self.index == 1 || self.index == 2
    }

    /// Returns the number of black tiles on the board
    pub fn nb_black(&self) -> usize {
        self.nb_tiles(PlayerColor::Black)
    }

    /// Returns the number of white tiles on the board
    pub fn nb_white(&self) -> usize {
        self.nb_tiles(PlayerColor::White)
    }

    /// Counts the number of tiles of a given color on the board
    pub fn nb_tiles(&self, color: PlayerColor) -> usize {
        self.tiles.iter()
            .filter(|x| **x == color)
            .count()
    }

    /// Update the board to reflect a move. The board is not updated if a MoveErr is raised
    pub fn play_move(&mut self, mv: &Mv) -> Result<(), MoveErr> {
        self.check_move(mv)?;
        self.play_move_unchecked(mv);
        Ok(())
    }

    /// Update the board without checking if the move is possible. Faster by avoiding checks,
    /// though it should not be used outside internal methods
    pub fn play_move_unchecked(&mut self, mv: &Mv) {
        let other_color = !mv.player_color;

        if !mv.is_passive && (self[mv.mid()] == other_color || self[mv.to] == other_color){
            if let Some(next) = mv.next() {
                self[next] = *other_color;
            }
        }

        self[mv.from] = Tile::Empty;
        self[mv.mid()] = Tile::Empty;
        self[mv.to] = *mv.player_color;
    }

    /// Asserts the sub-move is valid on this board
    pub fn check_move(&self, mv: &Mv) -> Result<(), MoveErr> {
        assert!(mv.check_valid().is_ok());

        let (same, diff) = self.effected_coords(mv);

        if self[mv.from] != mv.player_color && mv.is_passive {
            Err(MoveErr::NoPassiveTile)
        } else if self[mv.from] != mv.player_color && !mv.is_passive {
            Err(MoveErr::NoAggressiveTile)
        } else if same.len() != 0 && mv.is_passive {
            Err(MoveErr::PassiveBlockedByTile(same[0]))
        } else if same.len() != 0 && !mv.is_passive {
            Err(MoveErr::AggressiveBlockedBySelf(same[0]))
        } else if diff.len() != 0 && mv.is_passive {
            Err(MoveErr::PassiveBlockedByTile(diff[0]))
        } else if diff.len() > 1 && !mv.is_passive {
            Err(MoveErr::AggressiveBlockedByDoubleTile((diff[0], diff[1])))
        } else {
            Ok(())
        }
    }

    /// Returns 2 vectors, representing the tiles of same and different player being effected by
    /// the move. Empty tiles and the "from" tile are not returned
    fn effected_coords(&self, mv: &Mv) -> (Vec<Coord>, Vec<Coord>) {
        let mut same = Vec::with_capacity(3);
        let mut diff = Vec::with_capacity(3);

        let mut push_vec = |c| {
            if self[c] == mv.player_color {
                same.push(c);
            } else if self[c] != Tile::Empty {
                diff.push(c);
            }
        };

        push_vec(mv.to);

        if mv.dist() == 2 {
            push_vec(mv.mid());
        }

        // Only push (to + 1) for aggressive moves
        if !mv.is_passive {
            if let Some(c) = mv.next() {
                push_vec(c);
            }
        }

        (same, diff)
    }

    /// Generates all partial moves a player could make
    ///
    /// These moves are guarenteed to be legal, assuming a mirroring move exists, though the
    /// mirroring move isn't checked for. Use `.gen_moves()` on [Table] for legal moves only
    pub fn gen_mv(&self, color: PlayerColor, is_passive: bool) -> Vec<Mv> {
        let all_directions = MvVec::gen_all();

        self.tiles.iter().enumerate()
            .filter(|(_, x)| **x == color)
            .map(|(i, _)| Coord::from(self.index, i/4+1, i%4+1).unwrap())
            .map(|from| {
                all_directions.iter()
                    .filter_map(|mvvec| {
                        if let Some(to) = from.add(*mvvec) {
                            let mv = Mv { from, to, is_passive, player_color: color };
                            Some(mv)
                        } else { None }
                    })
                    .filter(|mv| self.check_move(mv).is_ok())
                    .collect::<Vec<Mv>>()
            })
            .flatten()
            .collect()
    }
}

/// Indexes the board with a [Coord].
///
/// This accounts for 1-indexing, though does not check if this is the correct board to index
impl std::ops::Index<Coord> for Board {
    type Output = Tile;

    fn index(&self, index: Coord) -> &Self::Output {
        let r = index.row - 1;
        let c = index.col - 1;

        &self.tiles[r * 4 + c]
    }
}

/// Human-readable display for a board
///
/// Mostly intended for debugging. Home board indicated in the top left. For example, board 1 (top
/// left) at the start looks like:
///
/// ```text
/// B 1 2 3 4
/// 1 B B B B  B
/// 2 . . . .  │
/// 3 . . . .  │
/// 4 W W W W  W
/// ```
impl std::fmt::Display for Board {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let home = if self.is_black_homeboard() { 'B' } else { 'W' };

        write!(f, "{} 1 2 3 4\n\
                1 {} {} {} {}  B\n\
                2 {} {} {} {}  │\n\
                3 {} {} {} {}  │\n\
                4 {} {} {} {}  W",
            home,
            self.tiles[0x0].as_char(), self.tiles[0x1].as_char(),
            self.tiles[0x2].as_char(), self.tiles[0x3].as_char(),
            self.tiles[0x4].as_char(), self.tiles[0x5].as_char(),
            self.tiles[0x6].as_char(), self.tiles[0x7].as_char(),
            self.tiles[0x8].as_char(), self.tiles[0x9].as_char(),
            self.tiles[0xa].as_char(), self.tiles[0xb].as_char(),
            self.tiles[0xc].as_char(), self.tiles[0xd].as_char(),
            self.tiles[0xe].as_char(), self.tiles[0xf].as_char(),
        )
    }
}

/// Indexes the board with a [Coord].
///
/// This accounts for 1-indexing, though does not check if this is the correct board to index
impl std::ops::IndexMut<Coord> for Board {
    fn index_mut(&mut self, index: Coord) -> &mut Self::Output {
        let r = index.row - 1;
        let c = index.col - 1;

        &mut self.tiles[r * 4 + c]
    }
}

impl Table {
    /// Creates a new table with boards initialised to their starting positions
    pub fn new() -> Self {
        Self {
            boards: [Board::init(1), Board::init(2), Board::init(3), Board::init(4)],
        }
    }

    /// Update the table to reflect a move. The table is not updated if a MoveErr is raised
    pub fn play_move(&mut self, m: &Move) -> Result<(), MoveErr> {
        let is_black = m.player_color.is_black();
        let passive_board = m.passive.board();

        if is_black && !(passive_board == 1 || passive_board == 2) {
            Err(MoveErr::PassiveNotOnHomeBoard)
        } else if !is_black && !(passive_board == 3 || passive_board == 4) {
            Err(MoveErr::PassiveNotOnHomeBoard)
        } else {
            m.check_valid()?;

            let p = m.passive.board() - 1;
            let a = m.aggressive.board() - 1;

            self.boards[p].check_move(&m.passive)?;
            self.boards[a].check_move(&m.aggressive)?;

            // Guaranteed to work, since we already checked them above
            self.boards[p].play_move_unchecked(&m.passive);
            self.boards[a].play_move_unchecked(&m.aggressive);
            Ok(())
        }
    }

    /// Returns the color of the player who's won or None otherwise
    ///
    /// Draws shouldn't be possible in shoubu, so this method returns as soon as it detects any
    /// win. Rule modifications, like clearing two boards, requires overloading this function
    pub fn winner(&self) -> Option<PlayerColor> {
        for board in self.boards.iter() {
            if board.winner().is_some() {
                return board.winner();
            }
        }
        None
    }

    /// Returns the number of tiles a player has remaining in total across all boards
    pub fn tiles_remaining(&self, color: PlayerColor) -> usize {
        self.boards.iter()
            .map(|b| b.nb_tiles(color))
            .sum()
    }

    /// Returns a unique hash for the markovian state of the table
    ///
    /// Unlike 64bit hashing, this one is guaranteed to be unique and can be used to reconstruct
    /// the board
    pub fn as_hash(&self) -> TableHash {
        let mut hash = 0_u128;

        for i in 0..64 {
            let b = i / 16;
            let j = i % 16;

            // Set non-empty tiles to 1 in right-64 bits
            if self.boards[b].tiles[j] != Tile::Empty {
                hash |= 1 << i;
            }

            // Set white tiles to 1 in left-64 bits
            if self.boards[b].tiles[j] == Tile::White {
                hash |= 1 << i+64;
            }
        }

        hash
    }

    /// Returns a Table with tiles laid out according to the hash
    ///
    /// Effectively decodes the hash. Can be used for compressed storage
    pub fn from_hash(hash: TableHash) -> Self {
        let mut table = Self::new();

        for i in 0..64 {
            let b = i / 16;
            let j = i % 16;

            let is_empty = (hash >> i & 1) == 0;
            let is_white = (hash >> i+64 & 1) == 1;

            table.boards[b].tiles[j] =
                if is_empty { Tile::Empty } else if is_white { Tile::White } else { Tile::Black };
        }

        table
    }

    /// Returns all legal moves a player could currently make
    ///
    /// This function will accurately return moves and handle errors, though it's the slowest
    /// function in this library
    pub fn gen_moves(&self, color: PlayerColor) -> Vec<Move> {
        let a1 = self.boards[0].gen_mv(color, false);
        let a2 = self.boards[1].gen_mv(color, false);
        let a3 = self.boards[2].gen_mv(color, false);
        let a4 = self.boards[3].gen_mv(color, false);

        let (left_passive, right_passive) =
            if color == PlayerColor::Black {
                let p1 = self.boards[0].gen_mv(color, true);
                let p2 = self.boards[1].gen_mv(color, true);

                (p2, p1)
            } else {
                let p3 = self.boards[2].gen_mv(color, true);
                let p4 = self.boards[3].gen_mv(color, true);

                (p3, p4)
            };

        // Filter out unmatched moves
        let right_flags: MvFlags = right_passive.iter().map(|m| m.delta()).collect();
        let left_flags: MvFlags = left_passive.iter().map(|m| m.delta()).collect();

        let left_aggressive = a2.into_iter().chain(a3)
            .filter(|mv| right_flags.contains(mv))
            .collect::<Vec<Mv>>();

        let right_aggressive = a1.into_iter().chain(a4)
            .filter(|mv| left_flags.contains(mv))
            .collect::<Vec<Mv>>();

        // Cross product passive Mv and aggressive Mv into Move
        let mut legal_moves = Vec::new();

        for mv in left_passive {
            right_aggressive.iter()
                .filter(|mirror| mv.delta() == mirror.delta())
                .map(|mirror| Move::new(color, mv, *mirror))
                .for_each(|m| legal_moves.push(m));
        }

        for mv in right_passive {
            left_aggressive.iter()
                .filter(|mirror| mv.delta() == mirror.delta())
                .map(|mirror| Move::new(color, mv, *mirror))
                .for_each(|m| legal_moves.push(m));
        }

        legal_moves
    }
}

/// Indexes the table with a [Coord].
///
/// This accounts for 1-indexing and finds the correct board as well
impl std::ops::Index<Coord> for Table {
    type Output = Tile;

    fn index(&self, coord: Coord) -> &Self::Output {
        &self.boards[coord.board - 1][coord]
    }
}

/// Indexes the table with a [Coord].
///
/// This accounts for 1-indexing and finds the correct board as well
impl std::ops::IndexMut<Coord> for Table {
    fn index_mut(&mut self, coord: Coord) -> &mut Self::Output {
        &mut self.boards[coord.board - 1][coord]
    }
}

/// Human-readable display for a table
///
/// Mostly intended for debugging. For example, the starting table looks like:
///
/// ```text
///   1 2 3 4     1 2 3 4
/// 1 B B B B  B  B B B B
/// 2 . . . .  │  . . . .
/// 3 . . . .  │  . . . .
/// 4 W W W W  │  W W W W
///   ═════════╪═════════
/// 1 B B B B  │  B B B B
/// 2 . . . .  │  . . . .
/// 3 . . . .  │  . . . .
/// 4 W W W W  W  W W W W
/// ```
impl std::fmt::Display for Table {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "  1 2 3 4     1 2 3 4\n\
                1 {b20} {b21} {b22} {b23}  B  {b10} {b11} {b12} {b13}\n\
                2 {b24} {b25} {b26} {b27}  │  {b14} {b15} {b16} {b17}\n\
                3 {b28} {b29} {b2a} {b2b}  │  {b18} {b19} {b1a} {b1b}\n\
                4 {b2c} {b2d} {b2e} {b2f}  │  {b1c} {b1d} {b1e} {b1f}
  ═════════╪═════════\n\
                1 {b30} {b31} {b32} {b33}  │  {b40} {b41} {b42} {b43}\n\
                2 {b34} {b35} {b36} {b37}  │  {b44} {b45} {b46} {b47}\n\
                3 {b38} {b39} {b3a} {b3b}  │  {b48} {b49} {b4a} {b4b}\n\
                4 {b3c} {b3d} {b3e} {b3f}  W  {b4c} {b4d} {b4e} {b4f}",
            b10 = self.boards[0].tiles[0x0].as_char(), b11 = self.boards[0].tiles[0x1].as_char(),
            b12 = self.boards[0].tiles[0x2].as_char(), b13 = self.boards[0].tiles[0x3].as_char(),
            b14 = self.boards[0].tiles[0x4].as_char(), b15 = self.boards[0].tiles[0x5].as_char(),
            b16 = self.boards[0].tiles[0x6].as_char(), b17 = self.boards[0].tiles[0x7].as_char(),
            b18 = self.boards[0].tiles[0x8].as_char(), b19 = self.boards[0].tiles[0x9].as_char(),
            b1a = self.boards[0].tiles[0xa].as_char(), b1b = self.boards[0].tiles[0xb].as_char(),
            b1c = self.boards[0].tiles[0xc].as_char(), b1d = self.boards[0].tiles[0xd].as_char(),
            b1e = self.boards[0].tiles[0xe].as_char(), b1f = self.boards[0].tiles[0xf].as_char(),
            // Board 2
            b20 = self.boards[1].tiles[0x0].as_char(), b21 = self.boards[1].tiles[0x1].as_char(),
            b22 = self.boards[1].tiles[0x2].as_char(), b23 = self.boards[1].tiles[0x3].as_char(),
            b24 = self.boards[1].tiles[0x4].as_char(), b25 = self.boards[1].tiles[0x5].as_char(),
            b26 = self.boards[1].tiles[0x6].as_char(), b27 = self.boards[1].tiles[0x7].as_char(),
            b28 = self.boards[1].tiles[0x8].as_char(), b29 = self.boards[1].tiles[0x9].as_char(),
            b2a = self.boards[1].tiles[0xa].as_char(), b2b = self.boards[1].tiles[0xb].as_char(),
            b2c = self.boards[1].tiles[0xc].as_char(), b2d = self.boards[1].tiles[0xd].as_char(),
            b2e = self.boards[1].tiles[0xe].as_char(), b2f = self.boards[1].tiles[0xf].as_char(),
            // Board 3
            b30 = self.boards[2].tiles[0x0].as_char(), b31 = self.boards[2].tiles[0x1].as_char(),
            b32 = self.boards[2].tiles[0x2].as_char(), b33 = self.boards[2].tiles[0x3].as_char(),
            b34 = self.boards[2].tiles[0x4].as_char(), b35 = self.boards[2].tiles[0x5].as_char(),
            b36 = self.boards[2].tiles[0x6].as_char(), b37 = self.boards[2].tiles[0x7].as_char(),
            b38 = self.boards[2].tiles[0x8].as_char(), b39 = self.boards[2].tiles[0x9].as_char(),
            b3a = self.boards[2].tiles[0xa].as_char(), b3b = self.boards[2].tiles[0xb].as_char(),
            b3c = self.boards[2].tiles[0xc].as_char(), b3d = self.boards[2].tiles[0xd].as_char(),
            b3e = self.boards[2].tiles[0xe].as_char(), b3f = self.boards[2].tiles[0xf].as_char(),
            // Board 4
            b40 = self.boards[3].tiles[0x0].as_char(), b41 = self.boards[3].tiles[0x1].as_char(),
            b42 = self.boards[3].tiles[0x2].as_char(), b43 = self.boards[3].tiles[0x3].as_char(),
            b44 = self.boards[3].tiles[0x4].as_char(), b45 = self.boards[3].tiles[0x5].as_char(),
            b46 = self.boards[3].tiles[0x6].as_char(), b47 = self.boards[3].tiles[0x7].as_char(),
            b48 = self.boards[3].tiles[0x8].as_char(), b49 = self.boards[3].tiles[0x9].as_char(),
            b4a = self.boards[3].tiles[0xa].as_char(), b4b = self.boards[3].tiles[0xb].as_char(),
            b4c = self.boards[3].tiles[0xc].as_char(), b4d = self.boards[3].tiles[0xd].as_char(),
            b4e = self.boards[3].tiles[0xe].as_char(), b4f = self.boards[3].tiles[0xf].as_char(),
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rand;
    use rand::Rng;

    #[test]
    fn table_hashing() {
        let mut rng = rand::thread_rng();

        for _ in 0..100 {
            let mut table = Table::new();

            for i in 0..64 {
                let b = i / 16;
                let j = i % 16;

                table.boards[b].tiles[j] = match rng.gen_range(0..3) {
                    0 => Tile::Empty,
                    1 => Tile::Black,
                    _ => Tile::White,
                };
            }

            let hash = table.as_hash();
            let decode = Table::from_hash(hash);

            let og_str = table.to_string();
            let decoded_str = decode.to_string();

            assert_eq!(og_str, decoded_str);
        }
    }
}
