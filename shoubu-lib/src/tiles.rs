//! Defines the most basic units in Shoubu
//!
//! Most objects in this module are trivially interoperable with other languages
use super::*;

/// A single tile on the board
///
/// Fields can be interpreted as isize
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum Tile {
    /// Always set to 1
    Black = 1,
    /// Always set to 0
    Empty = 0,
    /// Always set to -1
    White = -1,
}

/// An enumerator to indicate which player it is. Closely compatible with [Tile]
#[derive(Debug, Copy, Clone, PartialEq)]
pub enum PlayerColor {
    Black,
    White,
}

/// A 1-indexed coordinate on a [table](crate::table::Table)
///
/// Boards are indexed like [cartesian
/// quadrants](https://en.wikipedia.org/wiki/Quadrant_(plane_geometry)). Boards are indexed like a
/// matrix, with (1,1) being the top left corner and (4,4) being the bottom right
///
/// Coordinates are strongly tied in with the [moves module](crate::moves). It provides a
/// higher-level API with additional checking
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct Coord {
    pub board: usize,
    pub row: usize,
    pub col: usize,
}

pub type CoordHash = u8;

impl Tile {
    pub fn as_char(self) -> char {
        match self {
            Tile::Black => 'B',
            Tile::Empty => '.',
            Tile::White => 'W',
        }
    }
}

impl std::cmp::PartialEq<PlayerColor> for Tile {
    fn eq(&self, other: &PlayerColor) -> bool {
        *self == **other
    }
}

impl PlayerColor {
    pub fn is_black(&self) -> bool {
        *self == PlayerColor::Black
    }
}

impl std::ops::Deref for PlayerColor {
    type Target = Tile;

    fn deref(&self) -> &Self::Target {
        match self {
            PlayerColor::Black => &Tile::Black,
            PlayerColor::White => &Tile::White,
        }
    }
}

/// Convert the player to the opposite color. So Black becomes White and vise versa
impl std::ops::Not for PlayerColor {
    type Output = Self;

    fn not(self) -> Self::Output {
        match self {
            Self::Black => Self::White,
            Self::White => Self::Black,
        }
    }
}

impl std::convert::TryFrom<Tile> for PlayerColor {
    type Error = &'static str;

    fn try_from(tile: Tile) -> Result<Self, Self::Error> {
        match tile {
            Tile::Black => Ok(PlayerColor::Black),
            Tile::White => Ok(PlayerColor::White),
            Tile::Empty => Err("Empty tile, no player's color here"),
        }
    }
}

impl Coord {
    /// Creates a new [Coord].
    ///
    /// Indicies are not adjusted. Make sure they're 1-indexed
    pub fn from(board: usize, row: usize, col: usize) -> Option<Self> {
        if 1 <= board && board <= 4 && 1 <= row && row <= 4 && 1 <= col && col <= 4 {
            Some(Self { board, row, col })
        } else {
            None
        }
    }

    /// Perturbe a coordinate by a [movement vector](MvVec). Returns None if the resulting
    /// coordinate is invalid
    pub fn add(self, rhs: MvVec) -> Option<Self> {
        let row = self.row as isize + rhs.row;
        let col = self.col as isize + rhs.col;

        if row < 1 || 4 < row || col < 1 || 4 < col {
            None
        } else {
            Some(Self {
                board: self.board,
                row: row as usize,
                col: col as usize,
            })
        }
    }

    /// Returns true if the coordinate is on the "left" of the table
    ///
    /// Boards 2 and 3, in terms of [cartesian
    /// quadrants](https://en.wikipedia.org/wiki/Quadrant_(plane_geometry)) are considered on the
    /// "left". Interfaces need to consider this when drawing, as it's used to check if a move is
    /// mirrored
    pub fn is_left_board(&self) -> bool {
        self.board == 2 || self.board == 3
    }

    /// Validates a coordinate exists. Remember these are 1-indexed
    pub fn is_valid(&self) -> bool {
        1 <= self.board && self.board <= 4
        && 1 <= self.row && self.row <= 4
        && 1 <= self.col && self.col <= 4
    }

    /// Returns a string representation of the coordinate's 1-indexed tuple
    pub fn display_tuple(&self) -> String {
        format!("({}, {}, {})", self.board, self.row, self.col)
    }

    /// Returns a unique hash for this Coord.
    ///
    /// Shoubu tables have 64 coordinates, which can be encoded using 6 bits. The two highest-order
    /// bits are not used.
    pub fn as_hash(&self) -> CoordHash {
        // Hash stored as zero-indexed
        (16 * (self.board - 1) + 4 * (self.row - 1) + (self.col - 1)) as u8
    }

    /// Decodes hash into Coord.
    pub fn from_hash(mut hash: CoordHash) -> Self {
        let board = hash / 16;
        hash -= board * 16;
        let row = hash / 4;
        hash -= row * 4;
        let col = hash;

        // Hash stored as zero-indexed
        Self {
            board: (board + 1).into(),
            row: (row + 1).into(),
            col: (col + 1).into(),
        }
    }
}

/// Calculates the change in row and column between two coordinates as a [movement vector](MvVec)
impl std::ops::Sub for Coord {
    type Output = MvVec;

    fn sub(self, other: Self) -> Self::Output {
        let sr = self.row as isize;
        let sc = self.col as isize;
        let or = other.row as isize;
        let oc = other.col as isize;

        MvVec {
            row: sr - or,
            col: sc - oc,
        }
    }
}

impl std::fmt::Display for Coord {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let get_char = |x: Coord| if *self == x { 'X' } else { '.' };

        let b10 = get_char(Coord::from(1, 1, 1).unwrap()); let b11 = get_char(Coord::from(1, 1, 2).unwrap());
        let b12 = get_char(Coord::from(1, 1, 3).unwrap()); let b13 = get_char(Coord::from(1, 1, 4).unwrap());
        let b14 = get_char(Coord::from(1, 2, 1).unwrap()); let b15 = get_char(Coord::from(1, 2, 2).unwrap());
        let b16 = get_char(Coord::from(1, 2, 3).unwrap()); let b17 = get_char(Coord::from(1, 2, 4).unwrap());
        let b18 = get_char(Coord::from(1, 3, 1).unwrap()); let b19 = get_char(Coord::from(1, 3, 2).unwrap());
        let b1a = get_char(Coord::from(1, 3, 3).unwrap()); let b1b = get_char(Coord::from(1, 3, 4).unwrap());
        let b1c = get_char(Coord::from(1, 4, 1).unwrap()); let b1d = get_char(Coord::from(1, 4, 2).unwrap());
        let b1e = get_char(Coord::from(1, 4, 3).unwrap()); let b1f = get_char(Coord::from(1, 4, 4).unwrap());

        let b20 = get_char(Coord::from(2, 1, 1).unwrap()); let b21 = get_char(Coord::from(2, 1, 2).unwrap());
        let b22 = get_char(Coord::from(2, 1, 3).unwrap()); let b23 = get_char(Coord::from(2, 1, 4).unwrap());
        let b24 = get_char(Coord::from(2, 2, 1).unwrap()); let b25 = get_char(Coord::from(2, 2, 2).unwrap());
        let b26 = get_char(Coord::from(2, 2, 3).unwrap()); let b27 = get_char(Coord::from(2, 2, 4).unwrap());
        let b28 = get_char(Coord::from(2, 3, 1).unwrap()); let b29 = get_char(Coord::from(2, 3, 2).unwrap());
        let b2a = get_char(Coord::from(2, 3, 3).unwrap()); let b2b = get_char(Coord::from(2, 3, 4).unwrap());
        let b2c = get_char(Coord::from(2, 4, 1).unwrap()); let b2d = get_char(Coord::from(2, 4, 2).unwrap());
        let b2e = get_char(Coord::from(2, 4, 3).unwrap()); let b2f = get_char(Coord::from(2, 4, 4).unwrap());

        let b30 = get_char(Coord::from(3, 1, 1).unwrap()); let b31 = get_char(Coord::from(3, 1, 2).unwrap());
        let b32 = get_char(Coord::from(3, 1, 3).unwrap()); let b33 = get_char(Coord::from(3, 1, 4).unwrap());
        let b34 = get_char(Coord::from(3, 2, 1).unwrap()); let b35 = get_char(Coord::from(3, 2, 2).unwrap());
        let b36 = get_char(Coord::from(3, 2, 3).unwrap()); let b37 = get_char(Coord::from(3, 2, 4).unwrap());
        let b38 = get_char(Coord::from(3, 3, 1).unwrap()); let b39 = get_char(Coord::from(3, 3, 2).unwrap());
        let b3a = get_char(Coord::from(3, 3, 3).unwrap()); let b3b = get_char(Coord::from(3, 3, 4).unwrap());
        let b3c = get_char(Coord::from(3, 4, 1).unwrap()); let b3d = get_char(Coord::from(3, 4, 2).unwrap());
        let b3e = get_char(Coord::from(3, 4, 3).unwrap()); let b3f = get_char(Coord::from(3, 4, 4).unwrap());

        let b40 = get_char(Coord::from(4, 1, 1).unwrap()); let b41 = get_char(Coord::from(4, 1, 2).unwrap());
        let b42 = get_char(Coord::from(4, 1, 3).unwrap()); let b43 = get_char(Coord::from(4, 1, 4).unwrap());
        let b44 = get_char(Coord::from(4, 2, 1).unwrap()); let b45 = get_char(Coord::from(4, 2, 2).unwrap());
        let b46 = get_char(Coord::from(4, 2, 3).unwrap()); let b47 = get_char(Coord::from(4, 2, 4).unwrap());
        let b48 = get_char(Coord::from(4, 3, 1).unwrap()); let b49 = get_char(Coord::from(4, 3, 2).unwrap());
        let b4a = get_char(Coord::from(4, 3, 3).unwrap()); let b4b = get_char(Coord::from(4, 3, 4).unwrap());
        let b4c = get_char(Coord::from(4, 4, 1).unwrap()); let b4d = get_char(Coord::from(4, 4, 2).unwrap());
        let b4e = get_char(Coord::from(4, 4, 3).unwrap()); let b4f = get_char(Coord::from(4, 4, 4).unwrap());

        write!(f, "  1 2 3 4     1 2 3 4\n\
                1 {b20} {b21} {b22} {b23}  B  {b10} {b11} {b12} {b13}\n\
                2 {b24} {b25} {b26} {b27}  │  {b14} {b15} {b16} {b17}\n\
                3 {b28} {b29} {b2a} {b2b}  │  {b18} {b19} {b1a} {b1b}\n\
                4 {b2c} {b2d} {b2e} {b2f}  │  {b1c} {b1d} {b1e} {b1f}
  ═════════╪═════════\n\
                1 {b30} {b31} {b32} {b33}  │  {b40} {b41} {b42} {b43}\n\
                2 {b34} {b35} {b36} {b37}  │  {b44} {b45} {b46} {b47}\n\
                3 {b38} {b39} {b3a} {b3b}  │  {b48} {b49} {b4a} {b4b}\n\
                4 {b3c} {b3d} {b3e} {b3f}  W  {b4c} {b4d} {b4e} {b4f}",
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rand;
    use rand::Rng;

    #[test]
    fn coord_one_indexed() {
        assert!(Coord::from(2, 3, 0).is_none());
        assert!(Coord::from(2, 4, 0).is_none());
        assert!(Coord::from(1, 4, 2).is_some());
    }

    #[test]
    fn coord_hashing_extremes() {
        let corner_1_coord = Coord::from(1, 1, 1).unwrap();
        let corner_2_coord = Coord::from(1, 1, 4).unwrap();
        let corner_3_coord = Coord::from(3, 4, 1).unwrap();
        let corner_4_coord = Coord::from(4, 4, 4).unwrap();

        let corner_1_hash: u8 = 0;
        let corner_2_hash: u8 = 3;
        let corner_3_hash: u8 = 44;
        let corner_4_hash: u8 = 63;

        assert_eq!(corner_1_hash, corner_1_coord.as_hash());
        assert_eq!(corner_2_hash, corner_2_coord.as_hash());
        assert_eq!(corner_3_hash, corner_3_coord.as_hash());
        assert_eq!(corner_4_hash, corner_4_coord.as_hash());
    }

    #[test]
    fn coord_hash_decode() {
        let mut rng = rand::thread_rng();
        let mut r = |x| rng.gen_range(1..=x);

        for _ in 0..100 {
            let coord = Coord::from(r(4), r(4), r(4)).unwrap();
            let hash = coord.as_hash();

            assert_eq!(coord, Coord::from_hash(hash));
        }
    }

    #[test]
    fn coord_one_indexed_display_tuples() {
        let c1 = Coord::from(1, 1, 1).unwrap();
        let c2 = Coord::from(2, 4, 1).unwrap();
        let c3 = Coord::from(4, 4, 4).unwrap();

        assert_eq!(&c1.display_tuple(), "(1, 1, 1)");
        assert_eq!(&c2.display_tuple(), "(2, 4, 1)");
        assert_eq!(&c3.display_tuple(), "(4, 4, 4)");
    }
}
