//! Objects concerning a single move in Shoubu.
//!
//! A full [Move] is made up of two smaller partial [Mv]s. One is passive and the other is
//! aggressive. [MvVec] provides a way to compare the direction of two [Mv]s. [MvFlags] are a
//! hyperfast way to compare sets of [MvVec]. At any point in this process, a [MoveErr] can be
//! raised.
//!
//! All structs and enums in this module are reexported at the root level.
use super::{PlayerColor, Coord};
use std::cmp::max;

/// The move, passive or aggressive, on a board. This is half of a full [Move] in Shoubu
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Mv {
    pub player_color: PlayerColor,
    pub is_passive: bool,
    pub from: Coord,
    pub to: Coord,
}

/// A movement vector, storing the change in position from making a move
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct MvVec {
    pub row: isize,
    pub col: isize,
}

/// A bitset of bitflags for all 16 possible movement vectors
///
/// There are 16 bitflags. They go ccw from the x-axis, similar to a unit circle. The 2-step
/// version of the 1-step move follows directly on the 1-step's right. Use the methods to convert
/// to a bool more easily
///
///```text
///    ┌Horizontal/vertical moves
///    │       ┌Diagonal moves
/// ┌──┴───┐┌──┴───┐
/// 1212121212121212
/// └┤└┤└┤└┤└┤└┤└┤└┤
///  │ │ │ │ │ │ │ └( 1, 1) bottom-right
///  │ │ │ │ │ │ └──( 1,-1) bottom-left
///  │ │ │ │ │ └────(-1,-1) top-left
///  │ │ │ │ └──────(-1, 1) top-right
///  │ │ │ └────────( 1, 0) down
///  │ │ └──────────( 0,-1) left
///  │ └────────────(-1, 0) up
///  └──────────────( 0, 1) right
///```
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct MvFlags(u16);

impl MvFlags {
    /// Creates a mvflag. Only one bit will be flipped
    ///
    /// This method does not check if the [Mv] itself is valid
    pub fn from(mv: &MvVec) -> Self {
        let (r, c) = (*mv).unwrap();

        let off =
                 if r == 0  && c == 1  { 15 }
            else if r == 0  && c == 2  { 14 }
            else if r == -1 && c == 0  { 13 }
            else if r == -2 && c == 0  { 12 }
            else if r == 0  && c == -1 { 11 }
            else if r == 0  && c == -2 { 10 }
            else if r == 1  && c == 0  { 9 }
            else if r == 2  && c == 0  { 8 }
            else if r == -1 && c == 1  { 7 }
            else if r == -2 && c == 2  { 6 }
            else if r == -1 && c == -1 { 5 }
            else if r == -2 && c == -2 { 4 }
            else if r == 1  && c == -1 { 3 }
            else if r == 2  && c == -2 { 2 }
            else if r == 1  && c == 1  { 1 }
            else                       { 0 };

        Self(1_u16 << off)
    }

    /// Returns true when a movement vector is present the flags
    pub fn contains(&self, mv: &Mv) -> bool {
        self.0 & *Self::from(&mv.delta()) != 0
    }

    pub fn is_single_right(&self) -> bool { (**self >> 15) & 1 != 0 }
    pub fn is_single_up(&self)    -> bool { (**self >> 13) & 1 != 0 }
    pub fn is_single_left(&self)  -> bool { (**self >> 11) & 1 != 0 }
    pub fn is_single_down(&self)  -> bool { (**self >>  9) & 1 != 0 }

    pub fn is_double_right(&self) -> bool { (**self >> 14) & 1 != 0 }
    pub fn is_double_up(&self)    -> bool { (**self >> 12) & 1 != 0 }
    pub fn is_double_left(&self)  -> bool { (**self >> 10) & 1 != 0 }
    pub fn is_double_down(&self)  -> bool { (**self >>  8) & 1 != 0 }

    pub fn is_single_up_right(&self)   -> bool { (**self >> 7) & 1 != 0 }
    pub fn is_single_up_left(&self)    -> bool { (**self >> 5) & 1 != 0 }
    pub fn is_single_down_left(&self)  -> bool { (**self >> 3) & 1 != 0 }
    pub fn is_single_down_right(&self) -> bool { (**self >> 1) & 1 != 0 }

    pub fn is_double_up_right(&self)   -> bool { (**self >> 6) & 1 != 0 }
    pub fn is_double_up_left(&self)    -> bool { (**self >> 4) & 1 != 0 }
    pub fn is_double_down_left(&self)  -> bool { (**self >> 2) & 1 != 0 }
    pub fn is_double_down_right(&self) -> bool { (**self >> 0) & 1 != 0 }
}

impl std::ops::Deref for MvFlags {
    type Target = u16;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl FromIterator<Self> for MvFlags {
    fn from_iter<I: IntoIterator<Item=Self>>(iter: I) -> Self {
        let mut myself = Self(0);
        iter.into_iter().for_each(|x| myself |= *x);
        myself
    }
}

impl FromIterator<Mv> for MvFlags {
    fn from_iter<I: IntoIterator<Item=Mv>>(iter: I) -> Self {
        let mut myself = Self(0);
        iter.into_iter().for_each(|x| myself |= *x.delta().as_flag());
        myself
    }
}

impl FromIterator<MvVec> for MvFlags {
    fn from_iter<I: IntoIterator<Item=MvVec>>(iter: I) -> Self {
        let mut myself = Self(0);
        iter.into_iter().for_each(|x| myself |= *x.as_flag());
        myself
    }
}

impl std::ops::BitOrAssign<u16> for MvFlags {
    fn bitor_assign(&mut self, rhs: u16) {
        self.0 |= rhs;
    }
}

/// A single move in Shoubu, consisting of a passive and mirrored aggressive move
#[derive(Debug, Clone, PartialEq)]
pub struct Move {
    pub player_color: PlayerColor,
    pub passive: Mv,
    pub aggressive: Mv,
}

/// Every type of error than can occur in Shoubu
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum MoveErr {
    /// Move is made for the wrong player at this turn
    WrongPlayerTurn,
    /// Move has problems with its structure, not related to the rules
    MalformedMove,
    /// Passive coordinate specified does not have the player's tile there
    NoPassiveTile,
    /// Aggressive coordinate specified does not have the player's tile there
    NoAggressiveTile,
    /// Passive move's starting coordinate is not on the table
    InvalidPassiveFromCoordinate,
    /// Aggressive move's starting coordinate is not on the table
    InvalidAggressiveFromCoordinate,
    /// Passive move's ending coordinate is not on the table
    InvalidPassiveToCoordinate,
    /// Aggressive move's ending coordinate is not on the table
    InvalidAggressiveToCoordinate,
    /// Passive move starts and ends on different boards
    InconsistentBoardForPassive,
    /// Aggressive move starts and ends on different boards
    InconsistentBoardForAggressive,
    /// Aggressive move is not a mirror of the passive move
    MoveNotMirrored,
    /// Passive move is not made on one of the player's homeboards
    PassiveNotOnHomeBoard,
    /// Move attempts to make 3 steps, where the maximum in shoubu is 2
    ThreeStepMove,
    /// Passive move is blocked by another tile
    PassiveBlockedByTile(Coord),
    /// Aggressive move is blocked by own tile. Players cannot push their own tiles
    AggressiveBlockedBySelf(Coord),
    /// Aggressive move tried to push 2 opponent's tiles. A move can push at most 1 tile
    AggressiveBlockedByDoubleTile((Coord, Coord)),
}

pub type MoveHash = u32;

impl Mv {
    /// Returns the distance of a move, either 1 or 2
    pub fn dist(&self) -> usize {
        self.delta().dist()
    }

    /// Returns the change in rows and columns
    pub fn delta(&self) -> MvVec {
        self.to - self.from
    }

    /// Returns the direction of the change in rows and columns
    pub fn unit_delta(&self) -> MvVec {
        let delta = self.to - self.from;
        MvVec {
            row: delta.row.signum(),
            col: delta.col.signum(),
        }
    }

    /// Returns the coordinate 1 after the final position of a move, in the same direction
    ///
    /// None is returned if the coordinate is off the board. This coordinate is where the
    /// opponent's tiles will be pushed from an aggressive move
    pub fn next(&self) -> Option<Coord> {
        self.to.add(self.unit_delta())
    }

    /// Returns the middle coordinate on a 2-step move, or the resulting position on a 1-step move
    ///
    /// **This API can runtime error**. It assumes the resulting position of this move exists!
    ///
    /// If the move distance is 2, there's a coodinate between the starting and ending position,
    /// which is returned. If the move is only distance 1, the end position form this move is
    /// returned instead
    pub fn mid(&self) -> Coord {
        self.from.add(self.unit_delta()).unwrap()
    }

    /// Returns the index of the board this move takes place on
    pub fn board(&self) -> usize {
        self.from.board
    }

    /// Statelessly validates this sub-move
    ///
    /// This does not mean the entire move is valid. It may not be valid for the particular board
    /// it's referencing, or it may have no mirror moves, neither of which are checked here
    pub fn check_valid(&self) -> Result<(), MoveErr> {
        if !self.from.is_valid() && self.is_passive {
            Err(MoveErr::InvalidPassiveFromCoordinate)
        } else if !self.from.is_valid() {
            Err(MoveErr::InvalidAggressiveFromCoordinate)
        } else if !self.to.is_valid() && self.is_passive {
            Err(MoveErr::InvalidPassiveToCoordinate)
        } else if !self.to.is_valid() {
            Err(MoveErr::InvalidAggressiveToCoordinate)
        } else if self.from.board != self.to.board && self.is_passive {
            Err(MoveErr::InconsistentBoardForPassive)
        } else if self.from.board != self.to.board {
            Err(MoveErr::InconsistentBoardForAggressive)
        } else if !self.delta().is_valid() {
            Err(MoveErr::ThreeStepMove)
        } else {
            Ok(())
        }
    }

    /// Returns true if the move is on the "left" of the table
    ///
    /// This function doesn't check if both coordinates of a move are on the same board. Use an
    /// `.is_valid()` to check
    pub fn is_left_board(&self) -> bool {
        self.from.is_left_board()
    }
}

impl MvVec {
    /// Returns the "magnitude" of this movement vector
    ///
    /// It actually returns the number of steps this movement requires, not the euclidean distance
    pub fn dist(&self) -> usize {
        max(self.row.abs(), self.col.abs()) as usize
    }

    /// Deconstruct a movement vector to resemble a matrix index
    pub fn unwrap(self) -> (isize, isize) {
        (self.row, self.col)
    }

    /// Returns true of the [MvVec] is one of the 16 possible movement vectors
    pub fn is_valid(&self) -> bool {
        -2 <= self.row && self.row <= 2 && -2 <= self.col && self.col <= 2
    }

    /// Returns the [MvFlags] representation of this movement vector
    pub fn as_flag(&self) -> MvFlags {
        MvFlags::from(self)
    }

    /// Generates all possible movement vectors in Shoubu
    ///
    /// This will be 16 vectors. 8 for each possible direction and a 2-step version for each
    pub fn gen_all() -> [Self; 16] {
        [
            // Horizontal
            Self { row: 0,  col: 1  },
            Self { row: 0,  col: 2  },
            Self { row: -1, col: 0  },
            Self { row: -2, col: 0  },
            Self { row: 0,  col: -1 },
            Self { row: 0,  col: -2 },
            Self { row: 1,  col: 0  },
            Self { row: 2,  col: 0  },
            // Diagon Alley
            Self { row: 1,  col: 1  },
            Self { row: 2,  col: 2  },
            Self { row: 1,  col: -1 },
            Self { row: 2,  col: -2 },
            Self { row: -1, col: -1 },
            Self { row: -2, col: -2 },
            Self { row: -1, col: 1  },
            Self { row: -2, col: 2  },
        ]
    }
}

impl Move {
    /// Creates a new [Move]
    pub fn new(player_color: PlayerColor, passive: Mv, aggressive: Mv) -> Self {
        Self { player_color, passive, aggressive }
    }

    /// Returns the number of steps taken by this move
    pub fn dist(&self) -> usize {
        self.passive.dist()
    }

    /// Checks the validity of a move, assuming a completely empty table
    pub fn check_valid(&self) -> Result<(), MoveErr> {
        self.passive.check_valid()?;
        self.aggressive.check_valid()?;

        if self.passive.is_left_board() == self.aggressive.is_left_board() {
            Err(MoveErr::MoveNotMirrored)
        } else if self.passive.delta() != self.aggressive.delta() {
            Err(MoveErr::MoveNotMirrored)
        } else if !self.passive.is_passive || self.aggressive.is_passive {
            Err(MoveErr::MalformedMove)
        } else {
            Ok(())
        }
    }

    /// Returns a unique hash for this Move.
    ///
    /// Hashing is not checked, meaning the move may be illegal. However, given the table it was
    /// hashed on, it can be uniquely decoded.
    ///
    /// Internally it looks like below:
    ///
    ///```text
    /// .......444444333333222222111111P
    /// └─────┤└────┤└────┤└────┤└────┤└Player's color (0 == black)
    ///       │     │     │     │     └Coord of passive.from
    ///       │     │     │     └──────Coord of passive.to
    ///       │     │     └────────────Coord of aggressive.from
    ///       │     └──────────────────Coord of aggressive.to
    ///       └────────────────────────Unused bits. May have meaning in future versions
    ///```
    pub fn as_hash(&self) -> MoveHash {
        let mut hash = 0_u32;

        let color = (self.player_color == PlayerColor::White) as u32 & 1;
        let p_from = (self.passive.from.as_hash()    & 0x3F) as u32;
        let p_to   = (self.passive.to.as_hash()      & 0x3F) as u32;
        let a_from = (self.aggressive.from.as_hash() & 0x3F) as u32;
        let a_to   = (self.aggressive.to.as_hash()   & 0x3F) as u32;

        hash |= color;
        hash |= p_from << 0x1;
        hash |= p_to   << 0x7;
        hash |= a_from << 0xD;
        hash |= a_to   << 0x13;

        hash
    }

    /// Decodes hash into Move. The Move is not checked for legality.
    pub fn from_hash(hash: MoveHash) -> Self {
        let color = if hash & 1 == 0 { PlayerColor::Black } else { PlayerColor::White };
        let p_from = Coord::from_hash(((hash >> 0x1)  & 0x3F) as u8);
        let p_to   = Coord::from_hash(((hash >> 0x7)  & 0x3F) as u8);
        let a_from = Coord::from_hash(((hash >> 0xD)  & 0x3F) as u8);
        let a_to   = Coord::from_hash(((hash >> 0x13) & 0x3F) as u8);

        let passive = Mv {
            player_color: color,
            is_passive: true,
            from: p_from,
            to: p_to,
        };

        let aggressive = Mv {
            player_color: color,
            is_passive: false,
            from: a_from,
            to: a_to,
        };

        Self::new(color, passive, aggressive)
    }
}

impl std::fmt::Display for Move {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let get_char = |x: Coord| {
            if self.passive.from == x {
                'p'
            } else if self.passive.to == x {
                'P'
            } else if self.aggressive.from == x {
                'a'
            } else if self.aggressive.to == x {
                'A'
            } else {
                '.'
            }
        };

        let b10 = get_char(Coord::from(1, 1, 1).unwrap()); let b11 = get_char(Coord::from(1, 1, 2).unwrap());
        let b12 = get_char(Coord::from(1, 1, 3).unwrap()); let b13 = get_char(Coord::from(1, 1, 4).unwrap());
        let b14 = get_char(Coord::from(1, 2, 1).unwrap()); let b15 = get_char(Coord::from(1, 2, 2).unwrap());
        let b16 = get_char(Coord::from(1, 2, 3).unwrap()); let b17 = get_char(Coord::from(1, 2, 4).unwrap());
        let b18 = get_char(Coord::from(1, 3, 1).unwrap()); let b19 = get_char(Coord::from(1, 3, 2).unwrap());
        let b1a = get_char(Coord::from(1, 3, 3).unwrap()); let b1b = get_char(Coord::from(1, 3, 4).unwrap());
        let b1c = get_char(Coord::from(1, 4, 1).unwrap()); let b1d = get_char(Coord::from(1, 4, 2).unwrap());
        let b1e = get_char(Coord::from(1, 4, 3).unwrap()); let b1f = get_char(Coord::from(1, 4, 4).unwrap());

        let b20 = get_char(Coord::from(2, 1, 1).unwrap()); let b21 = get_char(Coord::from(2, 1, 2).unwrap());
        let b22 = get_char(Coord::from(2, 1, 3).unwrap()); let b23 = get_char(Coord::from(2, 1, 4).unwrap());
        let b24 = get_char(Coord::from(2, 2, 1).unwrap()); let b25 = get_char(Coord::from(2, 2, 2).unwrap());
        let b26 = get_char(Coord::from(2, 2, 3).unwrap()); let b27 = get_char(Coord::from(2, 2, 4).unwrap());
        let b28 = get_char(Coord::from(2, 3, 1).unwrap()); let b29 = get_char(Coord::from(2, 3, 2).unwrap());
        let b2a = get_char(Coord::from(2, 3, 3).unwrap()); let b2b = get_char(Coord::from(2, 3, 4).unwrap());
        let b2c = get_char(Coord::from(2, 4, 1).unwrap()); let b2d = get_char(Coord::from(2, 4, 2).unwrap());
        let b2e = get_char(Coord::from(2, 4, 3).unwrap()); let b2f = get_char(Coord::from(2, 4, 4).unwrap());

        let b30 = get_char(Coord::from(3, 1, 1).unwrap()); let b31 = get_char(Coord::from(3, 1, 2).unwrap());
        let b32 = get_char(Coord::from(3, 1, 3).unwrap()); let b33 = get_char(Coord::from(3, 1, 4).unwrap());
        let b34 = get_char(Coord::from(3, 2, 1).unwrap()); let b35 = get_char(Coord::from(3, 2, 2).unwrap());
        let b36 = get_char(Coord::from(3, 2, 3).unwrap()); let b37 = get_char(Coord::from(3, 2, 4).unwrap());
        let b38 = get_char(Coord::from(3, 3, 1).unwrap()); let b39 = get_char(Coord::from(3, 3, 2).unwrap());
        let b3a = get_char(Coord::from(3, 3, 3).unwrap()); let b3b = get_char(Coord::from(3, 3, 4).unwrap());
        let b3c = get_char(Coord::from(3, 4, 1).unwrap()); let b3d = get_char(Coord::from(3, 4, 2).unwrap());
        let b3e = get_char(Coord::from(3, 4, 3).unwrap()); let b3f = get_char(Coord::from(3, 4, 4).unwrap());

        let b40 = get_char(Coord::from(4, 1, 1).unwrap()); let b41 = get_char(Coord::from(4, 1, 2).unwrap());
        let b42 = get_char(Coord::from(4, 1, 3).unwrap()); let b43 = get_char(Coord::from(4, 1, 4).unwrap());
        let b44 = get_char(Coord::from(4, 2, 1).unwrap()); let b45 = get_char(Coord::from(4, 2, 2).unwrap());
        let b46 = get_char(Coord::from(4, 2, 3).unwrap()); let b47 = get_char(Coord::from(4, 2, 4).unwrap());
        let b48 = get_char(Coord::from(4, 3, 1).unwrap()); let b49 = get_char(Coord::from(4, 3, 2).unwrap());
        let b4a = get_char(Coord::from(4, 3, 3).unwrap()); let b4b = get_char(Coord::from(4, 3, 4).unwrap());
        let b4c = get_char(Coord::from(4, 4, 1).unwrap()); let b4d = get_char(Coord::from(4, 4, 2).unwrap());
        let b4e = get_char(Coord::from(4, 4, 3).unwrap()); let b4f = get_char(Coord::from(4, 4, 4).unwrap());

        write!(f, "  1 2 3 4     1 2 3 4\n\
                1 {b20} {b21} {b22} {b23}  B  {b10} {b11} {b12} {b13}\n\
                2 {b24} {b25} {b26} {b27}  │  {b14} {b15} {b16} {b17}\n\
                3 {b28} {b29} {b2a} {b2b}  │  {b18} {b19} {b1a} {b1b}\n\
                4 {b2c} {b2d} {b2e} {b2f}  │  {b1c} {b1d} {b1e} {b1f}
  ═════════╪═════════\n\
                1 {b30} {b31} {b32} {b33}  │  {b40} {b41} {b42} {b43}\n\
                2 {b34} {b35} {b36} {b37}  │  {b44} {b45} {b46} {b47}\n\
                3 {b38} {b39} {b3a} {b3b}  │  {b48} {b49} {b4a} {b4b}\n\
                4 {b3c} {b3d} {b3e} {b3f}  W  {b4c} {b4d} {b4e} {b4f}",
        )
    }
}

impl std::fmt::Display for MoveErr {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            MoveErr::WrongPlayerTurn => {
                write!(f, "Move does not match whose turn it is")
            },
            MoveErr::MalformedMove => {
                write!(f, "Move is malformed")
            }
            MoveErr::NoPassiveTile => {
                write!(f, "Passive tile referenced in the move does not exists on the board")
            }
            MoveErr::NoAggressiveTile => {
                write!(f, "Aggressive tile referenced in the move does not exists on the board")
            }
            MoveErr::InvalidPassiveFromCoordinate => {
                write!(f, "Starting coordinate passive move is off the table")
            }
            MoveErr::InvalidAggressiveFromCoordinate => {
                write!(f, "Starting coordinate aggressive move is off the table")
            }
            MoveErr::InvalidPassiveToCoordinate => {
                write!(f, "Ending coordinate passive move is off the table")
            }
            MoveErr::InvalidAggressiveToCoordinate => {
                write!(f, "Ending coordinate aggressive move is off the table")
            }
            MoveErr::InconsistentBoardForPassive => {
                write!(f, "Passive move's starting board does not match the ending board")
            }
            MoveErr::InconsistentBoardForAggressive => {
                write!(f, "Aggressive component's starting board does not match the ending board")
            }
            MoveErr::MoveNotMirrored => {
                write!(f, "Move is not mirrored by the same movement vector on the opposing side")
            }
            MoveErr::PassiveNotOnHomeBoard => {
                write!(f, "Passive move is not played on a homeboard")
            }
            MoveErr::ThreeStepMove => {
                write!(f, "Move makes 3 steps")
            }
            MoveErr::PassiveBlockedByTile(c) => {
                write!(f, "Passive move is blocked by tile at {}", c.display_tuple())
            }
            MoveErr::AggressiveBlockedBySelf(c) => {
                write!(f, "Aggressive move is blocked by own tile at {}", c.display_tuple())
            }
            MoveErr::AggressiveBlockedByDoubleTile((a, b)) => {
                write!(f, "Aggressive move is double-blocked by tiles at {} and {}",
                    a.display_tuple(), b.display_tuple())
            }
        }
    }
}

impl std::error::Error for MoveErr {}

#[cfg(test)]
mod tests {
    use super::*;
    use rand;
    use rand::Rng;

    #[test]
    fn move_hashing_extremes() {
        // Hardcoded extremes
        let corners_1 = 0b00000000110000100100010010000110;  // Top right on board 1 and 2
        let corners_2 = 0b00000001011001001100010010000110;  // Bottom left on board 3
        let flat_1    = 0b00000001111111101110111000101000;  // Straight down into bottom right on board 4
        let flat_2    = 0b00000000000000010000100110110110;  // Straight up to top left on board 1

        let corners_1_move = Move {
            player_color: PlayerColor::Black,
            passive: Mv {
                player_color: PlayerColor::Black,
                is_passive: true,
                from: Coord::from(1, 1, 4).unwrap(),
                to:   Coord::from(1, 3, 2).unwrap(),
            },
            aggressive: Mv {
                player_color: PlayerColor::Black,
                is_passive: false,
                from: Coord::from(2, 1, 3).unwrap(),
                to:   Coord::from(2, 3, 1).unwrap(),
            },
        };

        let corners_2_move = Move {
            player_color: PlayerColor::Black,
            passive: Mv {
                player_color: PlayerColor::Black,
                is_passive: true,
                from: Coord::from(1, 1, 4).unwrap(),
                to:   Coord::from(1, 3, 2).unwrap(),
            },
            aggressive: Mv {
                player_color: PlayerColor::Black,
                is_passive: false,
                from: Coord::from(3, 2, 3).unwrap(),
                to:   Coord::from(3, 4, 1).unwrap(),
            },
        };

        let flat_1_move = Move {
            player_color: PlayerColor::Black,
            passive: Mv {
                player_color: PlayerColor::Black,
                is_passive: true,
                from: Coord::from(2, 2, 1).unwrap(),
                to:   Coord::from(2, 4, 1).unwrap(),
            },
            aggressive: Mv {
                player_color: PlayerColor::Black,
                is_passive: false,
                from: Coord::from(4, 2, 4).unwrap(),
                to:   Coord::from(4, 4, 4).unwrap(),
            },
        };

        let flat_2_move = Move {
            player_color: PlayerColor::Black,
            passive: Mv {
                player_color: PlayerColor::Black,
                is_passive: true,
                from: Coord::from(2, 3, 4).unwrap(),
                to:   Coord::from(2, 1, 4).unwrap(),
            },
            aggressive: Mv {
                player_color: PlayerColor::Black,
                is_passive: false,
                from: Coord::from(1, 3, 1).unwrap(),
                to:   Coord::from(1, 1, 1).unwrap(),
            },
        };

        assert_eq!(corners_1, corners_1_move.as_hash());
        assert_eq!(corners_2, corners_2_move.as_hash());
        assert_eq!(flat_1, flat_1_move.as_hash());
        assert_eq!(flat_2, flat_2_move.as_hash());
    }

    #[test]
    fn move_hash_decode() {
        let mut rng = rand::thread_rng();
        let mut r = |x| rng.gen_range(1..=x);

        for _ in 0..100 {
            let player_color = if r(2) == 1 { PlayerColor::Black } else { PlayerColor::White };

            let p_board = r(4);
            let a_board = r(4);

            let random_move = Move {
                player_color,
                passive: Mv {
                    player_color,
                    is_passive: true,
                    from: Coord::from(p_board, r(4), r(4)).unwrap(),
                    to:   Coord::from(p_board, r(4), r(4)).unwrap(),
                },
                aggressive: Mv {
                    player_color,
                    is_passive: false,
                    from: Coord::from(a_board, r(4), r(4)).unwrap(),
                    to:   Coord::from(a_board, r(4), r(4)).unwrap(),
                },
            };

            let hash = random_move.as_hash();
            let decode_move = Move::from_hash(hash);

            assert_eq!(random_move, decode_move);
        }
    }
}
