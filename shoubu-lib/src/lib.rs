//! An all-inclusive library to get started playing the board game Shoubu!
//!
//! ## Shoubu rules
//!
//! Rules for the original game can be found
//! [here](https://www.smirkandlaughter.com/_files/ugd/693f33_ac912a3c391e4644a4d62a60ee2de749.pdf).
//! In summary, you make a move up to two tiles in any one direction on one of your home boards.
//! That's your "passive move", so it cannot push any tiles. Next you must make a move on either
//! board of the opposing shade in the exact same direction. This is your "aggressive move" and it
//! can push up to 1 of your opponents tiles. You cannot push your own tiles or more than 1
//! opponent's tile, even on the aggressive move. The goal of the game is to remove all 4 of your
//! opponent's stones from any one board
//!
//! We add a few unmentioned rules
//!  - No suicide moves
//!  - Passing isn't possible
//!
//! It may be a player ends up with no moves on their turn. If this is the case, handling it is to
//! your own discretion. This library only considers a "win" when all stones of a player are
//! removed from any single board
//!
//! ## Navigation
//!
//! This library exports all objects at its root and generally allows public access to most
//! functions. Doc comments will mention if any functions have a chance of failing. Generally
//! higher-level API will perform all necessary checks, though it may be slower
//!
//! A [Game] contains two [players](game::Player) and a [Table]. This table is then broken down
//! into 4 [boards](table::Board). Each board contains 16 [tiles](Tile) in a 4x4 layout. Boards are
//! indexed by 1-indexed coordinates ([Coord]) in a counter-clockwise manner like cartesian
//! [quadrants](https://en.wikipedia.org/wiki/Quadrant_(plane_geometry)). The top left corner on a
//! board correspondents to coordinate (1,1), while the bottom right is (4,4)
//!
//! The [tiles] module contains very basic definitions of [coordinates](tiles::Coord) and
//! [tiles](tiles::Tile). For increased interoperability, the Black tiles is defined as `1`, the
//! White tiles as `-1` and empty tiles are `0`
//!
//! The [moves] module defines every part of a move and provides quick state-less validation. It
//! also provides the [MoveErr] enum which is raised in response to invalid moves in other modules
//!
//! The [table] module stores a snapshot of the game. It's sub-divided into boards and generally
//! has accessible fields. It does not provide any visual API beyond a Debug, so direct-field
//! access is allowed for building an interface
//!
//! The [game] module is the highest-level module. It bundles all previous modules into a safe
//! well-tested wrapper and stores additional data about the game, including the move history
//!
//! When building interfaces and other not-particularly time sensitive applications, the [Game]
//! interface may be all you need. For high-performance bots and specialized training, consider
//! using the unchecked API from [tables](table::Table) directly
pub mod tiles;
pub mod moves;
pub mod table;
pub mod game;

pub use crate::tiles::{Tile, PlayerColor, Coord, CoordHash};
pub use crate::moves::{Move, MoveErr, Mv, MvVec, MvFlags, MoveHash};
pub use crate::table::{Table, TableHash};
pub use crate::game::Game;
