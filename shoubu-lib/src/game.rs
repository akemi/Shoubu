//! A high-level safe wrapper around the entire library
use super::*;

#[derive(Debug, Copy, Clone)]
/// A history entry from one turn
///
/// A State Action Reward bundled together. Aimed at bots trying to create heuristic evaluation
pub struct History {
    pub move_hash: MoveHash,
    pub table_hash: TableHash,
    end_result: Option<PlayerColor>,
}

impl History {
    pub fn new(r#move: MoveHash, table: TableHash, end_result: Option<PlayerColor>) -> Self {
        Self {
            move_hash: r#move,
            table_hash: table,
            end_result,
        }
    }

    pub fn r#move(&self) -> Move {
        Move::from_hash(self.move_hash)
    }

    pub fn table(&self) -> Table {
        Table::from_hash(self.table_hash)
    }

    pub fn player_color(&self) -> PlayerColor {
        self.r#move().player_color
    }

    pub fn winner(&self) -> Option<PlayerColor> {
        self.end_result
    }

    pub fn set_end(&mut self, winner: PlayerColor) {
        self.end_result = Some(winner);
    }
}

impl std::fmt::Display for History {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let winner = if self.winner().is_none() {
            "Nobody yet"
        } else if self.winner().unwrap() == PlayerColor::Black {
            "Black"
        } else {
            "White"
        };

        write!(f, "Winner: {winner}\nMove ====\n{}\nTable ====\n{}", self.r#move(), self.table())
    }
}

/// One of two players in Shoubu
///
/// Most API from [Player] is re-exported in a more usable interface from [Game]
#[derive(Clone)]
pub struct Player {
    pub color: PlayerColor,
    pub history: Vec<History>,
}

impl Player {
    pub fn new(color: PlayerColor) -> Self {
        Self {
            color,
            history: Vec::new(),
        }
    }

    pub fn turns_taken(&self) -> usize {
        self.history.len()
    }
}

impl std::fmt::Debug for Player {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Player")
            .field("color", &self.color)
            .field("history", &format!("Vec length {}", self.history.len()))
            .finish()
    }
}

/// An all-inclusive wrapper implementing a Shoubu game
///
/// Interfaces are highly encouraged to implement their logic using the [Game] API. However, this
/// API does not provide visuals. Look into reading [tables](crate::table::Table) directly for
/// that
///
/// This interface may be slow and not achieve memory requirements of high-performance bots. Such
/// bots should use [tables](crate::table::Table) directly to avoid the safety overhead provided by
/// [Game]
#[derive(Clone)]
pub struct Game {
    pub black_player: Player,
    pub white_player: Player,
    pub table: Table,
    pub history: Vec<History>,
}

impl Game {
    /// Creates a new [Game], with the table in its starting position
    pub fn new() -> Self {
        Self {
            black_player: Player::new(PlayerColor::Black),
            white_player: Player::new(PlayerColor::White),
            table: Table::new(),
            history: Vec::new(),
        }
    }

    /// Returns the number of turns taken this game
    pub fn turns_taken(&self) -> usize {
        self.history.len()
    }

    /// Returns the number of tiles a player has left on the table
    pub fn tiles_remaining(&self, player: PlayerColor) -> usize {
        self.table.tiles_remaining(player)
    }

    /// Returns the color of the player whose turn it is. Black always plays first
    pub fn player_turn(&self) -> PlayerColor {
        if self.turns_taken() % 2 == 0 {
            PlayerColor::Black
        } else {
            PlayerColor::White
        }
    }

    /// Returns the color of the player who's won, if any
    pub fn winner(&self) -> Option<PlayerColor> {
        self.table.winner()
    }

    /// Returns true if a player has won
    pub fn is_win(&self) -> bool {
        self.winner().is_some()
    }

    /// Attempts to make a move
    ///
    /// If an Err is raised, the game will not have updated anything. If Ok is returned, the game
    /// will have updated to the next move
    pub fn play_move(&mut self, m: Move) -> Result<(), MoveErr> {
        if *m.player_color != self.player_turn() {
            Err(MoveErr::WrongPlayerTurn)
        } else {
            self.table.play_move(&m)?;

            let hist = History::new(m.as_hash(), self.table.as_hash(), self.table.winner());

            if self.player_turn() == PlayerColor::Black {
                self.black_player.history.push(hist);
            } else {
                self.white_player.history.push(hist);
            }

            self.history.push(hist);
            Ok(())
        }
    }

    /// Returns all legal moves the current player could make
    ///
    /// This function will accurately return moves and handle errors, though it's the slowest
    /// function in this library
    pub fn generate_moves(&self) -> Vec<Move> {
        self.table.gen_moves(self.player_turn())
    }

    /// Restores the game to a previous state
    ///
    /// `nb_moves` is the number of moves back, so 0 won't undo anything. If the number of moves to
    /// undo is greater than the number of moves made, the game is reset.
    ///
    /// You cannot undo an undo! Forward-history is lost, so store a backup elsewhere if that's
    /// important
    pub fn undo(&mut self, nb_moves: usize) {
        if nb_moves >= self.turns_taken() {
            *self =  Self::new();
        } else {
            self.history.truncate(self.turns_taken() - nb_moves);
        }
    }

    /// Update the history with the end result
    ///
    /// Sets the winner for all previous states in the history, based on the winner of the game.
    /// This function does nothing if no player has won the game yet
    pub fn backprop_state(&mut self) {
        if let Some(winner) = self.winner() {
            self.history.iter_mut().for_each(|h| h.set_end(winner));
        }
    }
}

impl std::fmt::Debug for Game {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Game")
            .field("black_player", &self.black_player)
            .field("white_player", &self.white_player)
            .field("table", &self.table.as_hash())
            .field("history", &format!("Vec length {}", self.history.len()))
            .finish()
    }
}
