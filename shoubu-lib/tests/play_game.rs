use shoubu::*;
use shoubu::PlayerColor::*;
use shoubu::MoveErr::*;

type Try<T> = Result<T, Box<dyn std::error::Error>>;

fn create_move(color: PlayerColor, p_from: Coord, p_to: Coord, a_from: Coord) -> Move {
    let a_to = a_from.add(p_to - p_from).unwrap();
    create_move_full(color, p_from, p_to, a_from, a_to)
}

fn create_move_full(
    color: PlayerColor,
    p_from: Coord,
    p_to: Coord,
    a_from: Coord,
    a_to: Coord,
) -> Move {
    let passive = Mv {
        player_color: color,
        is_passive: true,
        from: p_from,
        to: p_to,
    };

    let aggressive = Mv {
        player_color: color,
        is_passive: false,
        from: a_from,
        to: a_to,
    };

    Move::new(color, passive, aggressive)
}

// Remember, these are one indexed
fn coord(board: usize, row: usize, col: usize) -> Coord {
    Coord::from(board, row, col).unwrap()
}

#[test]
fn play_game() -> Try<()> {
    let mut game = Game::new();

    let mmv = create_move(Black, coord(2,1,1), coord(2,2,1), coord(1,1,3));
    assert!(game.play_move(mmv).is_ok());

    // Wrong turn
    let mmv = create_move(Black, coord(2,1,1), coord(2,2,1), coord(1,1,3));
    assert_eq!(game.play_move(mmv), Err(WrongPlayerTurn));

    // Wrong turn
    let mmv = create_move(Black, coord(2,1,2), coord(2,2,2), coord(1,1,1));
    assert_eq!(game.play_move(mmv), Err(WrongPlayerTurn));

    // Wrong tiles
    let mmv = create_move(White, coord(3,1,4), coord(3,2,3), coord(4,1,3));
    assert_eq!(game.play_move(mmv), Err(NoPassiveTile));

    // Made on the same board
    let mmv = create_move(White, coord(2,4,4), coord(2,2,2), coord(2,4,3));
    assert!(game.play_move(mmv).is_err());

    // Both on the left
    let mmv = create_move(White, coord(3,4,4), coord(3,2,2), coord(2,4,3));
    assert_eq!(game.play_move(mmv), Err(MoveNotMirrored));

    // Passive isn't on a homeboard
    let mmv = create_move(White, coord(1,4,4), coord(1,2,2), coord(2,4,3));
    assert_eq!(game.play_move(mmv), Err(PassiveNotOnHomeBoard));

    // Move not mirrored: Passive is 1 step aggressive is 2
    let mmv = create_move_full(White, coord(4,4,4), coord(4,3,3), coord(2,4,3), coord(2,2,1));
    assert_eq!(game.play_move(mmv), Err(MoveNotMirrored));

    // Move not mirrored: Different directions
    let mmv = create_move_full(White, coord(4,4,4), coord(4,2,4), coord(2,4,3), coord(2,2,1));
    assert_eq!(game.play_move(mmv), Err(MoveNotMirrored));

    // One black captured
    let mmv = create_move(White, coord(4,4,4), coord(4,2,2), coord(2,4,3));
    assert!(game.play_move(mmv).is_ok());

    assert_eq!(game.tiles_remaining(Black), 15);
    assert_eq!(game.tiles_remaining(White), 16);

    // Wrong turn
    let mmv = create_move(White, coord(4,2,2), coord(4,1,3), coord(2,2,1));
    assert_eq!(game.play_move(mmv), Err(WrongPlayerTurn));

    // Wrong turn
    let mmv = create_move(White, coord(3,4,1), coord(3,3,1), coord(1,4,1));
    assert_eq!(game.play_move(mmv), Err(WrongPlayerTurn));

    // 0 step move
    let mmv = create_move(Black, coord(2,1,3), coord(2,1,3), coord(4,1,4));
    assert!(game.play_move(mmv).is_err());

    // 3 step move
    let mmv = create_move(Black, coord(2,1,3), coord(2,4,3), coord(4,1,4));
    assert_eq!(game.play_move(mmv), Err(ThreeStepMove));

    // ===============================================================
    // Play full game from this point
    // ===============================================================
    let mmv = create_move(Black, coord(1,2,3), coord(1,3,2), coord(2,1,2));
    assert!(game.play_move(mmv).is_ok());
    assert_eq!(game.tiles_remaining(Black), 15);
    assert_eq!(game.tiles_remaining(White), 15);

    let mmv = create_move(White, coord(3,4,1), coord(3,3,2), coord(4,2,2));
    assert!(game.play_move(mmv).is_ok());
    assert_eq!(game.tiles_remaining(Black), 14);
    assert_eq!(game.tiles_remaining(White), 15);

    let mmv = create_move(Black, coord(2,1,3), coord(2,2,3), coord(1,3,2));
    assert!(game.play_move(mmv).is_ok());
    assert_eq!(game.tiles_remaining(Black), 14);
    assert_eq!(game.tiles_remaining(White), 14);

    let mmv = create_move(White, coord(4,4,2), coord(4,2,2), coord(3,3,2));
    assert!(game.play_move(mmv).is_ok());
    assert_eq!(game.tiles_remaining(Black), 13);
    assert_eq!(game.tiles_remaining(White), 14);

    let mmv = create_move(Black, coord(1,1,1), coord(1,3,1), coord(2,2,1));
    assert!(game.play_move(mmv).is_ok());
    assert_eq!(game.tiles_remaining(Black), 13);
    assert_eq!(game.tiles_remaining(White), 13);

    let mmv = create_move(White, coord(3,4,2), coord(3,3,2), coord(4,2,2));
    assert!(game.play_move(mmv).is_ok());
    assert_eq!(game.tiles_remaining(Black), 12);
    assert_eq!(game.tiles_remaining(White), 13);

    let mmv = create_move(Black, coord(1,1,2), coord(1,2,3), coord(3,1,1));
    assert!(game.play_move(mmv).is_ok());
    assert_eq!(game.tiles_remaining(Black), 12);
    assert_eq!(game.tiles_remaining(White), 13);

    let mmv = create_move(White, coord(3,3,2), coord(3,3,3), coord(4,1,3));
    assert!(game.play_move(mmv).is_ok());
    assert_eq!(game.tiles_remaining(Black), 11);
    assert_eq!(game.tiles_remaining(White), 13);

    let mmv = create_move(Black, coord(2,2,3), coord(2,3,4), coord(4,1,1));
    assert!(game.play_move(mmv).is_ok());
    assert_eq!(game.tiles_remaining(Black), 11);
    assert_eq!(game.tiles_remaining(White), 13);

    let mmv = create_move(White, coord(4,1,4), coord(4,1,3), coord(2,4,2));
    assert!(game.play_move(mmv).is_ok());
    assert_eq!(game.tiles_remaining(Black), 10);
    assert_eq!(game.tiles_remaining(White), 13);

    let mmv = create_move(Black, coord(2,3,4), coord(2,4,4), coord(1,3,1));
    assert_eq!(game.play_move(mmv), Err(PassiveBlockedByTile(coord(2,4,4))));

    let mmv = create_move(Black, coord(1,1,4), coord(1,2,4), coord(2,3,4));
    assert!(game.play_move(mmv).is_ok());
    assert_eq!(game.tiles_remaining(Black), 10);
    assert_eq!(game.tiles_remaining(White), 12);

    let mmv = create_move(White, coord(4,4,3), coord(4,3,3), coord(3,3,3));
    assert!(game.play_move(mmv).is_ok());
    assert_eq!(game.tiles_remaining(Black), 10);
    assert_eq!(game.tiles_remaining(White), 12);

    let mmv = create_move(Black, coord(1,2,3), coord(1,2,1), coord(2,4,4));
    assert!(game.play_move(mmv).is_ok());
    assert_eq!(game.tiles_remaining(Black), 10);
    assert_eq!(game.tiles_remaining(White), 12);

    let mmv = create_move(White, coord(3,4,3), coord(3,2,1), coord(4,3,3));
    assert!(game.play_move(mmv).is_ok());
    assert_eq!(game.tiles_remaining(Black), 9);
    assert_eq!(game.tiles_remaining(White), 12);

    Ok(())
}
